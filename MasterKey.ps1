﻿#Requires -Modules ActiveDirectory

# Written by Asher Dratel
# Thanks to https://commondollars.wordpress.com/2013/11/22/unlocking-an-account-on-every-dc-without-forcing-replication/ for the guts
# Unlocks AD account/resets AD password across all DCs on the domain

# Only parameter is the AD name we're unlocking

[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True,Position=1,HelpMessage="Username to unlock")]
    [string]$ADName,

    [Parameter(Mandatory=$False,Position=2,HelpMessage="New temp password")]
    [ValidateNotNullOrEmpty()]
    [string]$NewPass,

    [Parameter(Mandatory=$False)]
    [switch]$Rebuild,

    [Parameter(Mandatory=$False)]
    [switch]$Force
)

$script:nameCheck = $false
$script:locked = $false

# Get the domain of the user running the script and initialize the array for the DC info
$domainName = (Get-ADDomain).distinguishedname
$script:dcList = @()

# Make a list of online DCs from the OU, no point in trying ones we can't connect to
function Set-DCList {
    # Running the connection check against the DCs can take a bit, even with just six or so,
    # so we'll "cache" a list of the active DCs in an xml file so we can easily re-import them as objects

    # Adjust your timeout for the DC list refresh here, in hours:
    $timespan = 24

    # Check when the XML was last updated
    if(Test-Path -Path "$PSScriptRoot\dclist.xml") {
        $lastUpdate = (Get-Item -Path "$PSScriptRoot\dclist.xml").LastWriteTime
    }

    # Building the DC list takes a minute, so we'll only do it if the list doesn't exist, is older than your defined timeout, or you've set the $Rebuild flag
    if ((-not (Test-Path -Path "$PSScriptRoot\dclist.xml")) -or ($lastUpdate -lt (Get-Date).AddHours("-$timespan")) -or ($Rebuild -eq $True)) {
        Write-Output "Generating DC list...`n"
        $allDCs = Get-ADComputer -Filter * -SearchBase "ou=Domain Controllers,$domainName"
        foreach ($srvr in $allDCs) {
            if (Test-Connection -ComputerName $srvr.DNSHostName -Count 1 -ErrorAction SilentlyContinue) {
                $script:dcList = $script:dcList + $srvr
            }
        }
        $script:dcList | Export-Clixml -Path "$PSScriptRoot\dclist.xml" -Force
    }
    else {
        $script:dcList = Import-Clixml -Path "$PSScriptRoot\dclist.xml"
        Write-Output "Using cached DC list, last updated at $($lastUpdate.ToShortTimeString()) on $($lastUpdate.ToShortDateString())`n"
        }
}


# Check to make sure the account actually exists
function Get-Name {
    try {
        # We don't actually care about the Get-ADUser output at all, we just need to make sure it exists and flag the nameCheck variable
        Get-ADUser -Identity $ADName | Out-Null
        $script:nameCheck = $true
    }
    catch {
        Write-Output "AD name incorrect."
        $script:ADName = Read-Host "Enter AD name"
        Get-Name
    }
}

# Checks to make sure the account is actually locked
function Get-LockStatus {
    # Looks up the AD user and pulls out the lockedout property, which is a boolean
    $lockCheck = Get-ADUser $ADName -Properties lockedout
    if ($lockCheck.lockedout) {
        $script:locked = $true
    }
}

# Unlock the account on all DCs
function Unlock-DC {
    $adUserObj = (Get-ADUser -Identity $ADName -Properties PasswordExpired,PasswordLastSet)
    if ($Force) {
        Write-Output "`nForce flag set for $ADName...`n"
    }
    else {
        Write-Output "`n$ADName is locked...`n"
    }
    Set-DCList
    foreach ($targetDC in $script:dcList) {
        try {
            Unlock-ADAccount -Identity $ADName -Server $targetDC.DNSHostName -ErrorAction SilentlyContinue
            Write-Output "Unlocked on $($targetDC.name)."
        }
        catch {
            Write-Warning "$($targetDC.name) is down/not responding."
        }
    }
    if ($adUserObj.PasswordExpired) {
        "`r"
        Write-Warning "Password expired. Last set: $($adUserObj.PasswordLastSet)"
    }
}

# Reset the AD password on all DCs
function Set-NewPassword {
    $SecPass = (ConvertTo-SecureString $NewPass -AsPlainText -Force)
    Write-Output "`nResetting password for $ADName...`n"
    DCList
    foreach ($targetDC in $script:dcList) {
        try {
            Set-ADAccountPassword -Identity $ADName -Server $targetDC.DNSHostName -NewPassword $SecPass -ErrorAction SilentlyContinue
            Set-ADUser -Identity $ADName -ChangePasswordAtLogon $true -Server $targetDC.DNSHostName
            Write-Output "Password reset on $($targetDC.name)."
        }
        catch {
            Write-Warning "$($targetDC.name) is down/not responding."
        }
    }
}

# Runs the functions
Get-Name
if ($nameCheck) {
    Get-LockStatus
    if ($locked -or $Force) {
        Unlock-DC
    }
    else {
        Write-Output "`nAccount $ADName is not locked."
    }
}
if ($NewPass) {
    Set-NewPassword
}