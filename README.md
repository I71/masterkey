# README #

MasterKey unlocks a user account across all DCs in an AD environment. It queries the user running the script to determine the AD Domain, as well as automatically building a list of online DCs to work against. The list of DCs is saved to dclist.xml in the same directory as the script, and by default is re-created if it's over 24 hours old when the script is run.

### Requirements ###

* MasterKey requires the ActiveDirectory module for Powershell

### Initial setup ###

* As of 2/17/2017 Masterkey comes in plain ol' script form as well as module form. I recommend using the module!
* To set up the module, move the UnlockADUser folder to your PowerShell module directory (can be found by entering $env:PSModulePath in the console), then use the following command to import the module:
```
#!powershell

Import-Module UnlockADUser
```

### Masterkey usage ###

* Example code assumes Masterkey has been imported as a module
* All MasterKey needs is the samaccountname to unlock, for example: 
```
#!powershell

Unlock-ADUser john.smith
```
 will unlock user john.smith across all DCs
* The newPass flag will reset the user's AD password to the specified one after unlocking the account, for example: 
```
#!powershell

Unlock-ADUser jane.doe -newPass "Password"
```
 will unlock Jane Doe's account and reset her password to "Password" across all DCs
* The rebuild flag will force the recreation of the dclist.xml file

### Adjusting settings ###

* To adjust when the dclist.xml file is re-written, modify the $timespan variable found in the DCList/Set-DCList function.